package com.dexter.imock

import com.dexter.imock.model.Scenario
import org.scalatest.{Matchers, WordSpec}
import io.circe.syntax._
import io.circe.parser._

class EncodingTests extends WordSpec with Matchers {

  val scenarioStr1 = """{"scenarioId":"test","method":"GET","segments":[],"params":{},"headers":{}}"""
  val scenarioStr2 = """{"scenarioId":"test","method":"GET","segments":["dsad"],"params":{"key":"sadsa","value":"asddsa"},"headers":{"key":"asdsa","value":"ads"},"body":"sad","responseBody":""}"""

  "Circe" should {
    "decode scenario1 good" in {
      decode[Scenario](scenarioStr1).fold(
        fa => {
          println(fa.getMessage)
          fail()
        },
        fb => fb.scenarioId shouldBe "test"
      )
    }
    "decode scenario2 good" in {
      decode[Scenario](scenarioStr2).fold(
        fa => {
          println(fa.getMessage)
          fail()
        },
        fb => fb.scenarioId shouldBe "test"
      )
    }
  }

}
