package com.dexter.imock

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import com.dexter.imock.client.{HttpSpecMockClient, MailgunClient}
import com.dexter.imock.controller.{GoogleController, InterfaceMockController, ResourceController}
import com.dexter.imock.service.{CookiesService, ResourceService}
import org.mockito.Mockito.when
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.duration._

abstract class GenericTestClass extends WordSpec with Matchers with ScalatestRouteTest with MockitoSugar {

  implicit def default(implicit system: ActorSystem) = RouteTestTimeout(5.seconds)

  val correctCookiesName = "correctCookiesName"

  val cookiesService: CookiesService = mock[CookiesService]
  when(cookiesService.COOK_NAME).thenReturn(correctCookiesName)

  val specClient: HttpSpecMockClient = mock[HttpSpecMockClient]
  val mailgunClient: MailgunClient = mock[MailgunClient]
  val resourceService = new ResourceService(cookiesService, specClient, mailgunClient)
  val interfaceController = new InterfaceMockController(cookiesService)
  val resourceController = new ResourceController(cookiesService, resourceService)
  val googleController = new GoogleController()

  val routes: Route = resourceController.route ~ interfaceController.route ~ googleController.route

}
