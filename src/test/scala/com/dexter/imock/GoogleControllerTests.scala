package com.dexter.imock

class GoogleControllerTests extends GenericTestClass {

  "Receive responce from google" should {
    "be successful" in {
      Post("/google/enter/success") ~> googleController.route ~> check {
        status.intValue should be(200)
      }
    }
  }

}
