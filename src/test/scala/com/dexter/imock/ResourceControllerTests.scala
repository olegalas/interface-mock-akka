package com.dexter.imock

import akka.http.scaladsl.model.headers.{Cookie, HttpCookie, HttpCookiePair, RawHeader}
import akka.http.scaladsl.model.{ContentTypes, DateTime}
import com.dexter.imock.client.SpecMockBadRequest
import com.dexter.imock.model.SuccessResponses.{GetScenarioOkResponse, GetSpecOkResponse, LoginOkResponse}
import com.dexter.imock.model._
import org.mockito.Matchers.{any, eq => eqq}
import org.mockito.Mockito.when

import scala.concurrent.Future

class ResourceControllerTests extends GenericTestClass {

  import cookiesService._

  val correctSpecId = "correctSpecId" // googleId
  val correctTokenId = "correctTokenId"
  val correctScenarioId = "correctScenarioId"
  val correctCookiesValue = "correctCookiesValue"

  val incorrectSpecId = "incorrectSpecId"
  val incorrectScenarioId = "wrongScenarioId"

  val segments = List("segment1", "segment2")
  val params: Map[String, String] = Map("param1" -> "valueParam1", "param2" -> "valueParam2")
  val headerss: Map[String, String] = Map("header1" -> "headerParam1", "header2" -> "headerParam2")
  val body = Some("some body")
  val responseBody = Some("some response body")
  val correctScenario = Scenario(correctScenarioId, "GET", segments, params, headerss, body, responseBody)
  val incorrectScenario = Scenario(incorrectScenarioId, "GET", segments, params, headerss, body, responseBody)

  val scenario = Scenario(correctScenarioId, "GET", Nil, Map(), Map(), Some(""), Some(""))
  val spec = Specification(correctSpecId, correctTokenId, List(scenario))

  when(specClient.getSpec(eqq(correctSpecId))).thenReturn(Future.successful(Right(spec)))
  when(specClient.getSpec(eqq(incorrectSpecId))).thenReturn(Future.successful(Left(SpecMockBadRequest("wrong spec id"))))
  when(specClient.putSpec(any(), any()))
    .thenReturn(Future.successful(Right(correctSpecId)))
    .thenReturn(Future.successful(Left(SpecMockBadRequest("duplicated spec id"))))
  when(specClient.postSpec(any(), any())).thenReturn(Future.successful(Right("OK")))
  when(specClient.putScenario(any(), eqq(correctScenario))(any())).thenReturn(Future.successful(Right()))
  when(specClient.putScenario(any(), eqq(incorrectScenario))(any())).thenReturn(Future.successful(Left(SpecMockBadRequest("duplicated scenario id"))))
  when(specClient.getScenario(any(), eqq(correctScenarioId))).thenReturn(Future.successful(Right(correctScenario)))
  when(specClient.getScenario(any(), eqq(incorrectScenarioId))).thenReturn(Future.successful(Left(SpecMockBadRequest("wrong scenario id"))))
  when(specClient.deleteScenario(any(), eqq(correctScenarioId))).thenReturn(Future.successful(Right()))
  when(specClient.deleteScenario(any(), eqq(incorrectScenarioId))).thenReturn(Future.successful(Left(SpecMockBadRequest("wrong scenario id"))))

  val rawHeader = RawHeader("Set-Cookie", HttpCookie(cookiesService.COOK_NAME, "uniqueValue", path = Some("/"), expires = Some(DateTime.now + 100000)).toString)
  when(cookiesService.cookieHeader(eqq(correctSpecId))).thenReturn(rawHeader)
  when(cookiesService.cookieHeader(eqq(incorrectSpecId))).thenReturn(rawHeader)
  when(cookiesService.specId(eqq(HttpCookiePair(correctCookiesName, correctCookiesValue)))).thenReturn(correctSpecId)
  when(cookiesService.ifCookiesValid(eqq(HttpCookiePair(correctCookiesName, correctCookiesValue)))).thenReturn(true)

  "Login process" should {

    "be OK login" in {
      val correctLogin = Login(correctSpecId, correctTokenId)
      Post("/v1/login", correctLogin) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        header("Set-Cookie") match {
          case Some(header) =>
            val v = header.value.split(";")(0).split("=")
            val (name, value) = (v(0), v(1))

            if(name == null || name.isEmpty || value == null || value.isEmpty) fail("Cookie was not set")
            if(name != COOK_NAME) fail("Invalid cookies name")
            if(value == "deleted") fail("Cookie was deleted")
          case None => fail("Cookie was not set")
        }
        val body = responseAs[LoginOkResponse]
        body.specId shouldEqual correctSpecId
        status.intValue should be(200)
      }
    }

    "be OK registration" in {
      val incorrectLogin = Login(incorrectSpecId, "token")
      Post("/v1/login", incorrectLogin) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        header("Set-Cookie") match {
          case Some(header) =>
            val v = header.value.split(";")(0).split("=")
            val (name, value) = (v(0), v(1))

            if(name == null || name.isEmpty || value == null || value.isEmpty) fail("Cookie was not set")
            if(name != COOK_NAME) fail("Invalid cookies name")
            if(value == "deleted") fail("Cookie was deleted")
          case None => fail("Cookie was not set")
        }
        val body = responseAs[LoginOkResponse]
        body.specId shouldEqual incorrectSpecId
        status.intValue should be(200)
      }
    }

  }

  "Logout process" should {

    "be OK" in {
      Post("/v1/logout") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        response.headers.foreach(println)
        header("Set-Cookie") match {
          case Some(header) =>

            val v = header.value.split(";")(0).split("=")
            val (name, value) = (v(0), v(1))

            if(name == null || name.isEmpty || value == null || value.isEmpty) fail("Cookie was not set")
            if(name != COOK_NAME) fail("Invalid cookies name")
            if(value != "deleted") fail("Not deleted value in cookie")

          case None => // ok
        }
        status.intValue should be(200)
      }
    }

  }

  "Spec retrieving" should {

    "be OK" in {
      Get("/v1/spec/" + correctSpecId) ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        status.intValue should be(200)
        val spec = responseAs[GetSpecOkResponse]
        spec.specDTO.specId shouldEqual correctSpecId
        spec.specDTO.scenario.head shouldEqual correctScenarioId
      }
    }

    "be 500 with wrong specId" in {
      Get("/v1/spec/wrongSpecId") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        status.intValue should be(400)
      }
    }

    "be 401 without cookies" in {
      Get("/v1/spec/" + correctSpecId) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        status.intValue should be(401)
      }
    }

    "be 403 with wrong cookies" in {
      Get("/v1/spec/" + correctSpecId) ~> Cookie(COOK_NAME -> "wrong-value") ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        status.intValue should be(403)
      }
    }

  }

  "PUT Scenario process" should {

    "be OK" in {
      val segments = List("segment1", "segment2")
      val params = Map("param1" -> "valueParam1", "param2" -> "valueParam2")
      val headers = Map("header1" -> "headerParam1", "header2" -> "headerParam2")
      val body = Some("some body")
      val responseBody = Some("some response body")
      val successScenario = Scenario(correctScenarioId, "GET", segments, params, headers, body, responseBody)
      Put("/v1/scenario", successScenario) ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        status.intValue should be(200)
      }
    }

    "be 401 without cookies" in {
      val segments = List("segment1", "segment2")
      val params = Map("param1" -> "valueParam1", "param2" -> "valueParam2")
      val headers = Map("header1" -> "headerParam1", "header2" -> "headerParam2")
      val body = Some("some body")
      val responseBody = Some("some response body")
      val successScenario = Scenario(correctScenarioId, "GET", segments, params, headers, body, responseBody)
      Put("/v1/scenario", successScenario) ~> routes ~> check {
        status.intValue should be(401)
      }
    }

    "be 400 with invalid scenarioId" in {
      val segments = List("segment1", "segment2")
      val params = Map("param1" -> "valueParam1", "param2" -> "valueParam2")
      val headers = Map("header1" -> "headerParam1", "header2" -> "headerParam2")
      val body = Some("some body")
      val responseBody = Some("some response body")
      val invalidScenario = Scenario(incorrectScenarioId, "GET", segments, params, headers, body, responseBody)
      Put("/v1/scenario", invalidScenario) ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        status.intValue should be(400)
      }
    }

    "be 403 with invalid cookies" in {
      val segments = List("segment1", "segment2")
      val params = Map("param1" -> "valueParam1", "param2" -> "valueParam2")
      val headers = Map("header1" -> "headerParam1", "header2" -> "headerParam2")
      val body = Some("some body")
      val responseBody = Some("some response body")
      val successScenario = Scenario(correctScenarioId, "GET", segments, params, headers, body, responseBody)
      Put("/v1/scenario", successScenario) ~> Cookie(COOK_NAME -> "wrong-value") ~> routes ~> check {
        status.intValue should be(403)
      }
    }
  }

  "GET Scenario process" should {

    "be OK" in {
      Get("/v1/scenario/" + correctScenarioId) ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        status.intValue should be(200)
        val scenario = responseAs[GetScenarioOkResponse]
        scenario.scenario.scenarioId shouldEqual correctScenarioId
      }
    }

    "be 401 without cookies" in {
      Get("/v1/scenario/" + correctScenarioId) ~> routes ~> check {
        status.intValue should be(401)
      }
    }

    "be 400 with wrong scenarioId" in {
      Get("/v1/scenario/wrongScenarioId") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        status.intValue should be(400)
      }
    }

    "be 403 with wrong cookie value" in {
      Get("/v1/scenario/" + correctScenarioId) ~> Cookie(COOK_NAME -> "wrong-value") ~> routes ~> check {
        status.intValue should be(403)
      }
    }
  }

  "DELETE Scenario process" should {

    "be OK" in {
      Delete("/v1/scenario/" + correctScenarioId) ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        status.intValue should be(200)
      }
    }

    "be 401 without cookies" in {
      Delete("/v1/scenario/" + correctScenarioId) ~> routes ~> check {
        status.intValue should be(401)
      }
    }

    "be 400 with wrong scenarioId" in {
      Delete("/v1/scenario/wrongScenarioId") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> routes ~> check {
        status.intValue should be(400)
      }
    }

    "be 403 with wrong cookie value" in {
      Delete("/v1/scenario/" + correctScenarioId) ~> Cookie(COOK_NAME -> "wrong-value") ~> routes ~> check {
        status.intValue should be(403)
      }
    }

  }

  "Wrong path" should {
    "be failure with GET" in {
      Get("/v1/wrong/path") ~> routes ~> check {
        status.intValue should be(400)
      }
    }
    "be failure with PUT" in {
      Put("/v1/wrong/path") ~> routes ~> check {
        status.intValue should be(400)
      }
    }
    "be failure with POST" in {
      Post("/v1/wrong/path") ~> routes ~> check {
        status.intValue should be(400)
      }
    }
    "be failure with DELETE" in {
      Delete("/v1/wrong/path") ~> routes ~> check {
        status.intValue should be(400)
      }
    }
  }

}
