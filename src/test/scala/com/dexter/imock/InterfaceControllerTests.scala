package com.dexter.imock

import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.model.headers.{Cookie, HttpCookiePair}
import org.mockito.Matchers.{eq => eqq}
import org.mockito.Mockito.when

class InterfaceControllerTests extends GenericTestClass {

  import cookiesService._

  val correctCookiesValue = "correctCookiesValue"
  val incorrectCookiesValue = "incorrectCookiesValue"

  when(cookiesService.checkCookie(eqq(HttpCookiePair(cookiesService.COOK_NAME, correctCookiesValue)))).thenReturn(true)
  when(cookiesService.checkCookie(eqq(HttpCookiePair(cookiesService.COOK_NAME, incorrectCookiesValue)))).thenReturn(false)

  "Enter to the Root page" should {
    "be successful without cookies" in {
      Get("/") ~> interfaceController.route ~> check {
        contentType shouldEqual ContentTypes.`text/html(UTF-8)`
        status.intValue should be(200)
      }
    }
    "be with redirect 308 response code with admin cookies" in {
      Get("/") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> interfaceController.route ~> check {
        status.intValue should be(308)
      }
    }
    "be successful also with invalid cookies" in {
      Get("/") ~> Cookie(COOK_NAME -> incorrectCookiesValue) ~> interfaceController.route ~> check {
        contentType shouldEqual ContentTypes.`text/html(UTF-8)`
        status.intValue should be(200)
      }
    }
  }

  "Static files" should {
    "be got by GET request" in {
      Get("/static/favicon.ico") ~> interfaceController.route ~> check {
        status.intValue should be(200)
      }
    }
    "return 400 response code with wrong path" in {
      Get("/static/nonexisted") ~> interfaceController.route ~> check {
        status.intValue should be(400)
      }
    }
  }

  "Enter to the Home page" should {
    "be with redirect without cookies" in {
      Get("/home") ~> interfaceController.route ~> check {
        status.intValue should be(308)
      }
    }
    "be successful with admin cookies" in {
      Get("/home") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> interfaceController.route ~> check {
        contentType shouldEqual ContentTypes.`text/html(UTF-8)`
        status.intValue should be(200)
      }
    }
    "be with redirect with invalid cookies" in {
      Get("/home") ~> Cookie(COOK_NAME -> incorrectCookiesValue) ~> interfaceController.route ~> check {
        status.intValue should be(308)
      }
    }
  }

  "Enter to unknown page" should {
    "be successful without cookies" in {
      Get("/some/rout") ~> interfaceController.route ~> check {
        contentType shouldEqual ContentTypes.`text/html(UTF-8)`
        status.intValue should be(200)
      }
    }
    "be successful with admin cookies" in {
      Get("/some/rout") ~> Cookie(COOK_NAME -> correctCookiesValue) ~> interfaceController.route ~> check {
        contentType shouldEqual ContentTypes.`text/html(UTF-8)`
        status.intValue should be(200)
      }
    }
    "be successful with invalid cookies" in {
      Get("/some/rout") ~> Cookie(COOK_NAME -> incorrectCookiesValue) ~> interfaceController.route ~> check {
        contentType shouldEqual ContentTypes.`text/html(UTF-8)`
        status.intValue should be(200)
      }
    }
  }

}
