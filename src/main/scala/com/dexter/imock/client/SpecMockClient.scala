package com.dexter.imock.client

import akka.http.scaladsl.model.RequestEntity
import com.dexter.imock.model.{Scenario, Specification}

import scala.concurrent.Future

trait SpecMockClient {

  def getSpec(specId: String): Future[Either[SpecMockFailResponse, Specification]]
  def postSpec(specId: String, password: String): Future[Either[SpecMockFailResponse, String]]
  def putSpec(specId: String, tokenId: String): Future[Either[SpecMockFailResponse, String]]
  def getScenario(specId: String, scenarioId: String): Future[Either[SpecMockFailResponse, Scenario]]
  def putScenario(specId: String, scenario: Scenario)(implicit toReq: Scenario => RequestEntity): Future[Either[SpecMockFailResponse, Unit]]
  def postScenario(specId: String, scenario: Scenario)(implicit toReq: Scenario => RequestEntity): Future[Either[SpecMockFailResponse, Unit]]
  def deleteScenario(specId: String, scenarioId: String): Future[Either[SpecMockFailResponse, Unit]]

}
