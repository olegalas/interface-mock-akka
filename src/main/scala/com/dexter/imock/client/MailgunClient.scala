package com.dexter.imock.client

import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.stream.Materializer

import scala.concurrent.{ExecutionContext, Future}

class MailgunClient(url: String, from: String, api: String)(implicit system: ActorSystem, mat: Materializer, executionContext: ExecutionContext)
  extends GenericClient {

  def resetPasswordEmail(to: String, newPass: String): Future[Either[SpecMockFailResponse, String]] = {
    makeFromDataReq[String](url, Map(
      "from" -> from,
      "to" -> to,
      "subject" -> "Reset password email",
      "text" -> s"Your password to your dexmock account was reset to $newPass"
    ), BasicHttpCredentials("api", api))
  }

}
