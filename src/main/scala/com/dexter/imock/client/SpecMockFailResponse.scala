package com.dexter.imock.client

trait SpecMockFailResponse {
  def message: String
}

case class SpecMockBadRequest(reason: String) extends SpecMockFailResponse {
  override def message: String = reason
}
case object SpecMockNotFound extends SpecMockFailResponse {
  override def message: String = "Was not found"
}
case class SpecMockDefaultFailResponse(reason: String) extends SpecMockFailResponse {
  override def message: String = reason
}