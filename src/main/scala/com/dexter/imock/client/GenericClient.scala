package com.dexter.imock.client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.HttpMethods.{GET, POST}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.Materializer
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

abstract class GenericClient(implicit system: ActorSystem, mat: Materializer, executionContext: ExecutionContext) {

  private val LOG: Logger = LoggerFactory.getLogger(this.getClass)
  private val http = Http()
  implicit val unitToReq: Unit => HttpEntity = (u: Unit) => HttpEntity(`application/json`, "")

  protected def makeReq[Resp, Req](url: String, method: HttpMethod = GET, req: Option[Req] = None)
                                  (implicit um: Unmarshaller[ResponseEntity, Resp], toEntity: Req => HttpEntity)
  : Future[Either[SpecMockFailResponse, Resp]] = {

    LOG.info(s"Rest request")
    LOG.info(s"Request to : $url")
    LOG.info(s"Method : $method")
    LOG.info(s"Body : $req")

    http.singleRequest(
      HttpRequest(
        uri = url,
        method = method,
        entity = {
          req.map { model => toEntity(model).asInstanceOf[RequestEntity]
          } getOrElse HttpEntity.Empty
        }
      )).recover {
      case e: Exception =>
        LOG.error(s"Connection error - ${e.getMessage}")
        HttpResponse(status = StatusCodes.NotFound)
    }.flatMap(resp => handleResp(resp))
  }

  protected def makeFromDataReq[Resp](url: String, params: Map[String, String], authorization: BasicHttpCredentials)
                                          (implicit um: Unmarshaller[ResponseEntity, Resp]): Future[Either[SpecMockFailResponse, Resp]] = {

    LOG.info(s"FormData request")
    LOG.info(s"Request to : $url")
    LOG.info(s"Method : $POST")
    LOG.info(s"Params : $params")

    http.singleRequest(
      HttpRequest(
        uri = url,
        method = POST,
        headers = List(headers.Authorization(authorization)),
        entity = FormData(params).toEntity(HttpCharsets.`UTF-8`)
      )
    ).recover {
      case e: Exception =>
        LOG.error(s"Connection error. Reason - ${e.getMessage}")
        HttpResponse(status = StatusCodes.NotFound)
    }.flatMap(resp => handleResp(resp))

  }

  private def handleResp[Resp](resp: HttpResponse)(implicit um: Unmarshaller[ResponseEntity, Resp]):Future[Either[SpecMockFailResponse, Resp]] = {
    val responseCode = resp.status.intValue
    if (resp.status.isSuccess) {
      Unmarshal(resp.entity).to[Resp].map { result =>
        LOG.info(s"Was successful response with body :\n$result")
        Right(result)
      }
    } else {
      Unmarshal(resp.entity).to[String].map { reason =>
        responseCode match {
          case 404 =>
            LOG.warn(s"Was connection error or 404 response")
            Left(SpecMockNotFound)
          case 400 =>
            LOG.warn(s"Was 400 response. Bad request. Reason :\n$reason")
            Left(SpecMockBadRequest(reason))
          case _ =>
            LOG.warn(s"Was unsuccessful response code : $responseCode and body :\n$reason")
            Left(SpecMockDefaultFailResponse(reason))
        }
      }
    }
  }

}
