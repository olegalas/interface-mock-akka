package com.dexter.imock.client

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model._
import akka.stream.Materializer
import com.dexter.imock.model.{RegisterNewSpec, Scenario, Specification}

import scala.concurrent.{ExecutionContext, Future}

class HttpSpecMockClient(smAddress: String)
                        (implicit system: ActorSystem, mat: Materializer, executionContext: ExecutionContext)
  extends GenericClient with SpecMockClient {

  override def getSpec(specId: String): Future[Either[SpecMockFailResponse, Specification]] = {
    makeReq[Specification, Unit](smAddress + specId)
  }

  override def putSpec(specId: String, tokenId: String): Future[Either[SpecMockFailResponse, String]] = {
    makeReq[String, RegisterNewSpec](smAddress + specId, PUT, Some(RegisterNewSpec(tokenId)))
  }


  override def postSpec(specId: String, password: String): Future[Either[SpecMockFailResponse, String]] = {
    makeReq[String, String](smAddress + specId, POST, Some(password))
  }

  override def getScenario(specId: String, scenarioId: String): Future[Either[SpecMockFailResponse, Scenario]] = {
    makeReq[Scenario, Unit](s"$smAddress$specId/$scenarioId")
  }

  override def putScenario(specId: String, scenario: Scenario)(implicit toReq: Scenario => RequestEntity): Future[Either[SpecMockFailResponse, Unit]] = {
    makeReq[String, Scenario](s"$smAddress$specId/${scenario.scenarioId}", PUT, Some(scenario)).map { either =>
      either.map { _ => () }
    }
  }

  override def postScenario(specId: String, scenario: Scenario)(implicit toReq: Scenario => RequestEntity): Future[Either[SpecMockFailResponse, Unit]] = {
    makeReq[String, Scenario](s"$smAddress$specId/${scenario.scenarioId}", POST, Some(scenario)).map { either =>
      either.map { _ => () }
    }
  }

  override def deleteScenario(specId: String, scenarioId: String): Future[Either[SpecMockFailResponse, Unit]] = {
    makeReq[String, Unit](s"$smAddress$specId/$scenarioId", DELETE).map { either =>
      either.map { _ => () }
    }
  }
}
