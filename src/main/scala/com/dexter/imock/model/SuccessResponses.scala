package com.dexter.imock.model

import akka.http.scaladsl.model.{ContentTypeRange, HttpEntity, HttpResponse}
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import io.circe.{Decoder, Encoder, Json}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._
import io.circe.parser.decode

import scala.concurrent.Future
import scala.language.implicitConversions

object SuccessResponses {

  protected def jsonContentTypes: List[ContentTypeRange] = List(`application/json`)
  val OkResp = HttpResponse(status = OK, entity = HttpEntity(`application/json`, "OK".asJson.toString))

  sealed trait InterfaceSuccessResponse {
    protected val json: Json
    def toResponse: HttpResponse = {
      HttpResponse(
        status = OK,
        entity = HttpEntity(`application/json`, json.toString)
      )
    }
  }


  case class LoginOkResponse(specId: String) extends InterfaceSuccessResponse {
    val encode: Encoder[LoginOkResponse] = deriveEncoder[LoginOkResponse]
    val json: Json = encode(this)
    def toResponseWithHeaders(implicit header: String => RawHeader): HttpResponse = {
      HttpResponse(
        status = OK,
        headers = scala.collection.immutable.Seq(header(specId)),
        entity = HttpEntity(`application/json`, json.toString)
      )
    }
  }
  object LoginOkResponse {
    implicit val decoder: Decoder[LoginOkResponse] = deriveDecoder[LoginOkResponse]
    implicit def toRight[T](r: LoginOkResponse): Either[T, LoginOkResponse] = Right(r)

    implicit val unmarsh: FromEntityUnmarshaller[LoginOkResponse] = {
      Unmarshaller.stringUnmarshaller
        .forContentTypes(jsonContentTypes: _*)
        .flatMap { ctx => mat => j =>
          decode[LoginOkResponse](j).fold(Future.failed, Future.successful)
        }
    }
  }


  case class GetSpecOkResponse(specDTO: SpecDTO) extends InterfaceSuccessResponse {
    val encode: Encoder[GetSpecOkResponse] = deriveEncoder[GetSpecOkResponse]
    val json: Json = encode(this)
  }
  object GetSpecOkResponse {
    implicit val decoder: Decoder[GetSpecOkResponse] = deriveDecoder[GetSpecOkResponse]
    implicit def toRight[T](success: GetSpecOkResponse): Either[T, GetSpecOkResponse] =
      Right(success)
    implicit val unmarsh: FromEntityUnmarshaller[GetSpecOkResponse] = {
      Unmarshaller.stringUnmarshaller
        .forContentTypes(jsonContentTypes: _*)
        .flatMap { ctx => mat => j =>
          decode[GetSpecOkResponse](j).fold(Future.failed, Future.successful)
        }
    }
  }


  case class GetScenarioOkResponse(scenario: Scenario) extends InterfaceSuccessResponse {
    val encode: Encoder[GetScenarioOkResponse] = deriveEncoder[GetScenarioOkResponse]
    override val json: Json = encode(this)
  }
  object GetScenarioOkResponse {
    implicit val decoder: Decoder[GetScenarioOkResponse] = deriveDecoder[GetScenarioOkResponse]
    implicit def toRight[T](r: GetScenarioOkResponse): Either[T, GetScenarioOkResponse] = Right(r)
    implicit val unmarsh: FromEntityUnmarshaller[GetScenarioOkResponse] = {
      Unmarshaller.stringUnmarshaller
        .forContentTypes(jsonContentTypes: _*)
        .flatMap { ctx => mat => j =>
          decode[GetScenarioOkResponse](j).fold(Future.failed, Future.successful)
        }
    }
  }



}
