package com.dexter.imock.model

import akka.http.scaladsl.model.{HttpEntity, HttpResponse, StatusCode}
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model.StatusCodes._
import io.circe.syntax._

import scala.concurrent.Future
import scala.language.implicitConversions

object FailResponses {

  val UnauthorizedResp = HttpResponse(status = Unauthorized, entity = HttpEntity(`application/json`, "Cookie is missed".asJson.toString))

  sealed trait InterfaceFailResponse {
    protected val message: String
    protected val status: StatusCode
    def toResponse: HttpResponse = {
      HttpResponse(
        status = status,
        entity = HttpEntity(`application/json`, message.asJson.toString)
      )
    }

    implicit def toFutureLeft[T](err: InterfaceFailResponse): Future[Either[InterfaceFailResponse, T]] =
      Future.successful(
        Left(err)
      )
    implicit def toLeft[T](err: InterfaceFailResponse): Either[InterfaceFailResponse, T] =
      Left(err)
  }

  object EmptyLoginFailResponse extends InterfaceFailResponse {
    override val message: String = "Empty login"
    override val status: StatusCode = BadRequest
  }

  object SpecMockFailResponse extends InterfaceFailResponse {
    override val message: String = "Spec mock fail response. Try later or inform IT service"
    override val status: StatusCode = InternalServerError
  }

  object InvalidCookieFailResponse extends InterfaceFailResponse {
    override val message: String = "Invalid cookie"
    override val status: StatusCode = Forbidden
  }

  object EmptySpecIdFailResponse extends InterfaceFailResponse {
    override val message: String = "Empty specId"
    override val status: StatusCode = BadRequest
  }

  object InvalidSpecIdForSessionFailResponse extends InterfaceFailResponse {
    override val message: String = "Invalid specId for this session"
    override val status: StatusCode = BadRequest
  }

  object EmptyScenarioIdFailResponse extends InterfaceFailResponse {
    override val message: String = "Empty scenarioId"
    override val status: StatusCode = BadRequest
  }

  object InvalidSpecIdFailResponse extends InterfaceFailResponse {
    override val message: String = "Invalid specId"
    override val status: StatusCode = BadRequest
  }

  object InvalidScenarioIdFailResponse extends InterfaceFailResponse {
    override val message: String = "Invalid scenarioId"
    override val status: StatusCode = BadRequest
  }

}
