package com.dexter.imock.model

import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.parser.decode
import io.circe.syntax._
import io.circe.{Decoder, Encoder}

import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

sealed trait Models {
  protected def jsonContentTypes: List[ContentTypeRange] = List(`application/json`)
}

case class Login(googleId: String, tokenId: String) extends Models
object Login extends Models {

  implicit val decodeLogin: Decoder[Login] = deriveDecoder[Login]
  implicit val encodeLogin: Encoder[Login] = deriveEncoder[Login]

  implicit val unmarshLogin: FromEntityUnmarshaller[Login] = {
    Unmarshaller.stringUnmarshaller
      .forContentTypes(jsonContentTypes: _*)
      .flatMap { ctx => mat => json =>
        decode[Login](json).fold(Future.failed, Future.successful)
      }
  }

  implicit def marshalLogin(implicit executionContext: ExecutionContext): ToEntityMarshaller[Login] = {
    Marshaller.withFixedContentType(`application/json`) { login =>
      HttpEntity(`application/json`, login.asJson.toString)
    }
  }

}

case class Specification(specId: String, userTokenId: String, scenario: List[Scenario]) extends Models
object Specification extends Models {
  implicit val decodeSpecification: Decoder[Specification] = deriveDecoder[Specification]
  implicit val encodeSpecification: Encoder[Specification] = deriveEncoder[Specification]

  implicit val unmarshSpec: FromEntityUnmarshaller[Specification] = {
    Unmarshaller.stringUnmarshaller
      .forContentTypes(jsonContentTypes: _*)
      .flatMap { ctx => mat => json =>
        decode[Specification](json).fold(Future.failed, Future.successful)
      }
  }

}

case class SpecDTO(specId: String, scenario: List[String]) extends Models
object SpecDTO extends Models {
  implicit val decodeSpecDTO: Decoder[SpecDTO] = deriveDecoder[SpecDTO]
  implicit val encodeSpecDTO: Encoder[SpecDTO] = deriveEncoder[SpecDTO]

  implicit val specificationToSpecDTO: Specification => SpecDTO = s => SpecDTO(s.specId, s.scenario.map(_.scenarioId))

}

case class Scenario(
                     scenarioId:    String,
                     method:        String,
                     segments:      List[String],
                     params:        Map[String, String],
                     headers:       Map[String, String],
                     body:          Option[String],
                     responseBody:  Option[String]
                   ) extends Models
object Scenario extends Models {

  implicit val decodeScenario: Decoder[Scenario] = deriveDecoder[Scenario]
  implicit val encodeScenario: Encoder[Scenario] = deriveEncoder[Scenario]

  implicit val unmarshScenario: FromEntityUnmarshaller[Scenario] = {
    Unmarshaller.stringUnmarshaller
      .forContentTypes(jsonContentTypes: _*)
      .flatMap { ctx => mat => json =>
        decode[Scenario](json).fold(Future.failed, Future.successful)
      }
  }

  implicit def marshalScenario(implicit executionContext: ExecutionContext): ToEntityMarshaller[Scenario] = {
    Marshaller.withFixedContentType(`application/json`) { scenario =>
      HttpEntity(`application/json`, scenario.asJson.toString)
    }
  }

  implicit def toRequest(scenario: Scenario): RequestEntity = HttpEntity(`application/json`, scenario.asJson.toString)

}

case class RegisterNewSpec(userTokenId: String)extends Models
object RegisterNewSpec extends Models {

  implicit val decodeRegisterNewSpec: Decoder[RegisterNewSpec] = deriveDecoder[RegisterNewSpec]
  implicit val encodeRegisterNewSpec: Encoder[RegisterNewSpec] = deriveEncoder[RegisterNewSpec]

  implicit def marshalRegisterNewSpec(implicit executionContext: ExecutionContext): ToEntityMarshaller[RegisterNewSpec] = {
    Marshaller.withFixedContentType(`application/json`) { newSpec =>
      HttpEntity(`application/json`, newSpec.asJson.toString)
    }
  }

  implicit def toEntity(spec: RegisterNewSpec): HttpEntity = {
    HttpEntity(`application/json`, spec.asJson.toString)
  }
}
