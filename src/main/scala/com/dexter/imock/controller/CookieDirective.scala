package com.dexter.imock.controller

import akka.http.scaladsl.model.headers.HttpCookiePair
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.directives.BasicDirectives.{extract, provide}
import akka.http.scaladsl.server.directives.RouteDirectives.reject


import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.http.impl.util._

object CookieDirective {

  def myCookie(name: String) : Directive1[Option[HttpCookiePair]] = {
    println("============ MY COOKIE")
    extract(_.request.headers.collectFirst {
      case c: Cookie =>
        println("============Directive")
        c.cookies.foreach(println)
        val ll = c.cookies.find(_.name == name)
        println(s"===========Filtered $ll")
        ll
    }.flatten.map(c => {println(s"============ COOKIE $c"); c })).flatMap(provide)
  }

}
