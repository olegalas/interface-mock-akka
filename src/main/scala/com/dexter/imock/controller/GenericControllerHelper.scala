package com.dexter.imock.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes.{Forbidden, InternalServerError, MethodNotAllowed, BadRequest}
import akka.http.scaladsl.server.Directives.{complete, redirect}
import akka.http.scaladsl.server.{RequestEntityExpectedRejection, _}
import com.dexter.imock.util.FileTemplate
import org.slf4j.{Logger, LoggerFactory}

object GenericControllerHelper {

  val LOG: Logger = LoggerFactory.getLogger(GenericControllerHelper.getClass)

  implicit def myRejectionHandler: RejectionHandler =
    RejectionHandler.newBuilder()
      .handle { case RequestEntityExpectedRejection =>
        LOG.info("RequestEntityExpectedRejection")
        complete((BadRequest, "RequestEntityExpectedRejection"))
      }
      .handle { case UnsupportedRequestContentTypeRejection(supported) =>
        LOG.info("Unsupported types. Supports only $supported")
        complete((BadRequest, s"Unsupported types. Supports only $supported"))
      }
      .handle { case MalformedRequestContentRejection(message, ex) =>
        LOG.info(s"MalformedRequestContentRejection. Message - $message. Exception - $ex")
        complete((BadRequest, s"MalformedRequestContentRejection. Message - $message. Exception - $ex"))
      }
      .handle { case MissingCookieRejection(cookieName) =>
        LOG.info("MISSING COOKIE")
        redirect("/", StatusCodes.PermanentRedirect)
      }
      .handle { case AuthorizationFailedRejection =>
        complete((Forbidden, "You're out of your depth!"))
      }
      .handle { case ValidationRejection(msg, _) =>
        complete((InternalServerError, "That wasn't valid! " + msg))
      }
      .handleAll[MethodRejection] { methodRejections =>
      val names = methodRejections.map(_.supported.name)
      complete((MethodNotAllowed, s"Can't do that! Supported: ${names mkString " or "}!"))
    }  // If not found - return index.html with REACT routing
      .handleNotFound {
      complete(FileTemplate().response)
    }
      .result()

}
