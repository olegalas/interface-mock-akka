package com.dexter.imock.controller

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.server.Directives.{complete, path, pathPrefix, post}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.immutable.Seq

class GoogleController {

  private val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  private lazy val corsSettings: CorsSettings =
    CorsSettings.defaultSettings.withAllowedMethods(Seq(OPTIONS, POST, PUT, GET, DELETE))


  def route(implicit akkaSystem: ActorSystem): Route = {
    Route.seal {
      cors(corsSettings) {

        pathPrefix("google") {
          pathPrefix("enter") {
            path("success") {
              post {
                decodeRequest{
                  entity(as[String]){ str =>
                    LOG.info(s"Was received from google : $str")
                    complete("OK")
                  }
                }
              }
            }
          }
        }

      }
    }
  }

}
