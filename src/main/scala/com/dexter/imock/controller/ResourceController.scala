package com.dexter.imock.controller

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives.{complete, entity, path, _}
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.dexter.imock.model._
import com.dexter.imock.model.SuccessResponses._
import com.dexter.imock.model.FailResponses._
import com.dexter.imock.service.{CookiesService, ResourceService}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext

class ResourceController(cookiesService: CookiesService, resourceService: ResourceService)(implicit executionContext: ExecutionContext) {

  import cookiesService._

  private val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  private lazy val corsSettings: CorsSettings =
    CorsSettings.defaultSettings.withAllowedMethods(Seq(OPTIONS, POST, PUT, GET, DELETE))

  def route(implicit akkaSystem: ActorSystem): Route = {
    cors(corsSettings) {

      pathPrefix("v1") {
        path("login") {
          post {
            entity(as[Login]) { login =>
              complete {
                resourceService.doLogin(login).map {
                  case Left(err) => err.toResponse
                  case Right(login) => login.toResponseWithHeaders
                }
              }
            }
          }
        } ~ path("logout") {
          post {
            deleteCookie(name = COOK_NAME, path = "/") {
              complete(OkResp)
            }
          }
        } ~ path("spec" / Segment) { specId =>
          get {
            cookie(COOK_NAME) { cookie =>
              complete {
                resourceService.getSpec(cookie, specId).map {
                  case Left(err) => err.toResponse
                  case Right(spec) => spec.toResponse
                }
              }
            }
          } ~ get {
            complete(UnauthorizedResp)
          }
        } ~ path("scenario") {
          put {
            cookie(COOK_NAME) { cookie =>
              entity(as[Scenario]) { scenario =>
                complete {
                  resourceService.putScenario(cookie, scenario).map {
                    case Left(reason) => reason.toResponse
                    case Right(_) => OkResp
                  }
                }
              }
            }
          } ~ put {
            complete(UnauthorizedResp)
          }
        } ~ path("scenario" / Segment) { scenarioId =>
          get {
            cookie(COOK_NAME) { cookie =>
              complete {
                resourceService.getScenario(cookie, scenarioId).map {
                  case Left(reason) => reason.toResponse
                  case Right(scenario) => scenario.toResponse
                }
              }
            }
          } ~ get {
            complete(UnauthorizedResp)
          } ~ delete {
            cookie(COOK_NAME) { cookie =>
              complete {
                resourceService.deleteScenario(cookie, scenarioId).map {
                  case Left(reason) => reason.toResponse
                  case Right(_) => OkResp
                }
              }
            }
          } ~ delete {
            complete(UnauthorizedResp)
          }
        } ~ path(Segments) { segments =>
          LOG.warn(s"Wrong path v1${segments.map("/"+_).mkString}")
          complete(HttpResponse(StatusCodes.BadRequest))
        }
      }

    }
  }

}
