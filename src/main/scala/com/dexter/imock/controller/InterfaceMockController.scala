package com.dexter.imock.controller

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, cookie, get, path, _}
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.dexter.imock.service.CookiesService
import com.dexter.imock.util.FileTemplate
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.immutable._

class InterfaceMockController(cookiesService: CookiesService) {

  import GenericControllerHelper._
  import cookiesService._

  private val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  private lazy val corsSettings: CorsSettings =
    CorsSettings.defaultSettings.withAllowedMethods(Seq(OPTIONS, POST, PUT, GET, DELETE))

  def route(implicit akkaSystem: ActorSystem): Route = {
    Route.seal {
      cors(corsSettings) {

        // API for index.html
        path("") {
          get {

            cookie(COOK_NAME) { cookie =>
              if (checkCookie(cookie)) {
                redirect("/home", StatusCodes.PermanentRedirect)
              } else {
                complete(FileTemplate().response)
              }
            }

          } ~ get {

            complete(FileTemplate().response)

          }

          // API for static files
        } ~ path("static" / Segments) { segments =>
          get {
            extractRequest { request =>
              val path = segments.map(s => "/" + s).mkString
              if (path.endsWith("svg"))
                complete(FileTemplate("/static" + path, ContentType(MediaTypes.`image/svg+xml`)).response)
              else
                complete(FileTemplate("/static" + path, request.entity.contentType).response)
            }
          }
        } ~ path("home") {
          get {

            cookie(COOK_NAME) { cookie =>
              if (checkCookie(cookie)) {
                complete(FileTemplate().response)
              } else {
                redirect("/", StatusCodes.PermanentRedirect)
              }
            }

          }

        }
      }
    }

  }

}
