package com.dexter.imock.service

import akka.http.scaladsl.model.headers.HttpCookiePair
import com.dexter.imock.client.{HttpSpecMockClient, MailgunClient, SpecMockBadRequest, SpecMockDefaultFailResponse, SpecMockFailResponse, SpecMockNotFound}
import com.dexter.imock.model._
import com.dexter.imock.model.FailResponses._
import com.dexter.imock.model.SuccessResponses._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

class ResourceService(
                        cookiesService: CookiesService,
                        specClient: HttpSpecMockClient,
                        mailgunClient: MailgunClient
                     )
                     (implicit executionContext: ExecutionContext) {

  private val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  def doLogin(login: Login): Future[Either[InterfaceFailResponse, LoginOkResponse]] = {
    (login.googleId, login.tokenId) match {
      case (l, r) if l.isEmpty || r.isEmpty =>
        LOG.info("Was received empty login")
        EmptyLoginFailResponse
      case (googleId, tokenId) =>

        // Check if user registered
        LOG.info(s"Was received $login")
        specClient.getSpec(googleId).flatMap {

          case Left(failResponse) =>
            LOG.info(s"Failed to retrieved spec : $failResponse")
            failResponse match {
              // If spec was not found - register user
              case SpecMockBadRequest(_) =>
                LOG.info(s"Will register new spec")
                specClient.putSpec(googleId, tokenId).map {
                  case Left(reason) =>
                    LOG.error(s"Couldn't register new spec: $reason")
                    mapSpecMockResponse(reason)
                  case Right(_) =>
                    LOG.info(s"Was registered new spec")
                    LoginOkResponse(googleId)
                }

              case fr:SpecMockFailResponse =>
                LOG.warn(s"Unknown error: $fr")
                SpecMockFailResponse
            }

          case Right(spec) =>
            LOG.info(s"Success login. $spec")
            Future(Right(LoginOkResponse(spec.specId)))
        }
    }
  }

  def getSpec(cookie: HttpCookiePair, specId: String): Future[Either[InterfaceFailResponse, GetSpecOkResponse]] = {
    LOG.info(s"Was received getSpec by specId: $specId")
    (cookie, specId) match {
      case (c, _) if !cookiesService.ifCookiesValid(c) =>
        LOG.info(s"Invalid cookie - $c")
        InvalidCookieFailResponse
      case (_, s) if s.isEmpty =>
        LOG.info("Was empty specId")
        EmptySpecIdFailResponse
      case _ =>
        if(cookiesService.specId(cookie) == specId) {

          specClient.getSpec(specId).map {
            case Left(reason) =>
              LOG.error(s"Couldn't get spec: $reason")
              mapSpecMockResponse(reason, InvalidSpecIdFailResponse)
              SpecMockFailResponse
            case Right(specDto) =>
              LOG.info(s"Fetched spec : $specDto")
              GetSpecOkResponse(specDto)
          }

        } else {
          LOG.info("Invalid specId for this session")
          InvalidSpecIdForSessionFailResponse
        }
    }
  }

  def putScenario(cookie: HttpCookiePair, scenario: Scenario): Future[Either[InterfaceFailResponse, Unit]] = {
    LOG.info(s"Was received scenario for put : $scenario")
    (cookie, scenario) match {
      case (c, _) if !cookiesService.ifCookiesValid(c) =>
        LOG.info(s"Invalid cookie - $c")
        InvalidCookieFailResponse
      case (_, s) if s.scenarioId.isEmpty =>
        LOG.info("Was empty scenarioId")
        EmptyScenarioIdFailResponse
      case _ =>
        val specId = cookiesService.specId(cookie)
        specClient.putScenario(specId, scenario).map {
          case Left(reason) =>
            LOG.error(s"Couldn't put scenario: $reason")
            mapSpecMockResponse(reason, InvalidScenarioIdFailResponse)
          case Right(_) =>
            LOG.info("New scenario was successfully created")
            Right(())
        }
    }
  }

  def getScenario(cookie: HttpCookiePair, scenarioId: String): Future[Either[InterfaceFailResponse, GetScenarioOkResponse]] = {
    LOG.info(s"Get scenario by scenarioId : $scenarioId")
    (cookie, scenarioId) match {
      case (c, _) if !cookiesService.ifCookiesValid(c) =>
        LOG.info(s"Invalid cookie - $c")
        InvalidCookieFailResponse
      case (_, s) if s.isEmpty =>
        LOG.info("Was empty scenarioId")
        EmptyScenarioIdFailResponse
      case _ =>
        val specId = cookiesService.specId(cookie)
        specClient.getScenario(specId, scenarioId).map {
          case Left(reason) =>
            LOG.error(s"Couldn't get scenario: $reason")
            mapSpecMockResponse(reason, InvalidScenarioIdFailResponse)
          case Right(scenario) =>
            LOG.info(s"Fetched scenario : $scenario")
            GetScenarioOkResponse(scenario)
        }
    }
  }

  def deleteScenario(cookie: HttpCookiePair, scenarioId: String): Future[Either[InterfaceFailResponse, Unit]] = {
    LOG.info(s"Delete scenario by scenarioId : $scenarioId")
    (cookie, scenarioId) match {
      case (c, _) if !cookiesService.ifCookiesValid(c) =>
        LOG.info(s"Invalid cookie - $c")
        InvalidCookieFailResponse
      case (_, s) if s.isEmpty =>
        LOG.info("Was empty scenarioId")
        EmptyScenarioIdFailResponse
      case _ =>
        val specId = cookiesService.specId(cookie)
        specClient.deleteScenario(specId, scenarioId).map {
          case Left(reason) =>
            LOG.error(s"Couldn't delete spec: $reason")
            mapSpecMockResponse(reason, InvalidScenarioIdFailResponse)
          case Right(_) =>
            LOG.info("Scenario was deleted successfully")
            Right(())
        }
    }
  }

  def mapSpecMockResponse[T](specMockFailResponse: SpecMockFailResponse,
                          defaultResponse: InterfaceFailResponse = SpecMockFailResponse): Either[InterfaceFailResponse, T] = {
    specMockFailResponse match {
      case SpecMockBadRequest(_) => Left(defaultResponse)
      case SpecMockDefaultFailResponse(_) => SpecMockFailResponse
      case SpecMockNotFound => SpecMockFailResponse
      case _ => SpecMockFailResponse
    }
  }

}
