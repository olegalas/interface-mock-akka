package com.dexter.imock.service

import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

import akka.http.scaladsl.model.DateTime
import akka.http.scaladsl.model.headers.{HttpCookie, HttpCookiePair, RawHeader}

import scala.language.implicitConversions

class CookiesService(val COOK_NAME: String) {

  private val WEEK = 1000 * 60 * 60 * 24 * 7
  private val VALUES = new ConcurrentHashMap[String, String]

  def newCookie(specId: String): HttpCookie = {
    val uniqueValue = UUID.randomUUID().toString
    VALUES.put(uniqueValue, specId)
    HttpCookie(COOK_NAME, uniqueValue, path = Some("/"), expires = Some(DateTime.now + WEEK))
  }

  def checkCookie(cookie:  HttpCookiePair): Boolean =
    VALUES.containsKey(cookie.value)

  def removeCookies(cookie:  HttpCookiePair): Unit =
    VALUES.remove(cookie.value)

  def ifCookiesValid(cookie: HttpCookiePair): Boolean = checkCookie(cookie)

  def specId(cookie:  HttpCookiePair): String = VALUES.get(cookie.value)

  implicit def cookieHeader(specId: String): RawHeader = {
    RawHeader("Set-Cookie", newCookie(specId).toString())
  }

}
