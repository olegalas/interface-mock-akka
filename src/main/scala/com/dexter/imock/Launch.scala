package com.dexter.imock

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.dexter.imock.client.{HttpSpecMockClient, MailgunClient}
import com.dexter.imock.config.IMockConfig
import com.dexter.imock.controller.{GoogleController, InterfaceMockController, ResourceController}
import com.dexter.imock.service.{CookiesService, ResourceService}
import com.typesafe.config.ConfigFactory
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContextExecutor

object Launch extends App {

  val LOG: Logger = LoggerFactory.getLogger(Launch.getClass)

  implicit val system: ActorSystem = ActorSystem("interface-mock-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val mainConfig = ConfigFactory.load()
  val mailgunConfig = IMockConfig.mailgunConfig(mainConfig)
  val cookiesService =
    new CookiesService(mainConfig.getString("cookieName"))

  val specClient = new HttpSpecMockClient(mainConfig.getString("smAddress"))
  val mailgunClient = new MailgunClient(mailgunConfig.url, mailgunConfig.from, mailgunConfig.api)
  val resourceService = new ResourceService(cookiesService, specClient, mailgunClient)
  val iController = new InterfaceMockController(cookiesService)
  val rController = new ResourceController(cookiesService, resourceService)
  val gController = new GoogleController

  val bindingFuture = Http().bindAndHandle(rController.route ~ iController.route ~ gController.route, "localhost", 8082)

  bindingFuture
    .map(binding => {
      LOG.info("Server was run")
      LOG.info(s"Address ${binding.localAddress}")
    }).recover {
    case t:Throwable =>
      LOG.error("There was an error during starting server", t)
      System.exit(-1)
  }

}
