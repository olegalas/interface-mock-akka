package com.dexter.imock.util

import akka.http.scaladsl.model._
import org.slf4j.{Logger, LoggerFactory}

case class FileTemplate(
                          private val filename: String = "/index.html",
                          private val mediaType: ContentType = ContentType(MediaTypes.`text/html`, HttpCharsets.`UTF-8`)
                       ) {

  val STATIC_FOLDER = "/front-end/build"

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  def response: HttpResponse = try {
    LOG.info(s"Get file : $filename")
    val bytes = FileUtil.getFile(STATIC_FOLDER + filename)
    def entity: ResponseEntity = HttpEntity(mediaType, bytes)
    HttpResponse(entity = entity)
  } catch {
    case t:Throwable =>
      LOG.error(s"Was error during get file. ${t.getMessage}")
      HttpResponse(StatusCodes.BadRequest)
  }
}
