package com.dexter.imock.util

import scala.collection.immutable.Stream
import scala.language.postfixOps

object FileUtil {

  def getFile(path: String): Array[Byte] = {
    val source = getClass.getResourceAsStream(path)
    Stream.continually(source.read).takeWhile(-1 !=).map(_.toByte).toArray
  }

}
