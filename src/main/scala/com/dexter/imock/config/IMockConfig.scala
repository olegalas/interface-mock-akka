package com.dexter.imock.config

import com.typesafe.config._

object IMockConfig {

  def mailgunConfig(mainConfig: Config): MailgunConfig = MailgunConfig(mainConfig)

  case class MailgunConfig(
    url: String,
    from: String,
    api: String
  )
  object MailgunConfig {
    def apply(config: Config): MailgunConfig = {
      val mailgunConfig = config.getConfig("mailgunConfig")
      MailgunConfig(
        mailgunConfig.getString("url"),
        mailgunConfig.getString("from"),
        mailgunConfig.getString("api")
      )
    }
  }

}
