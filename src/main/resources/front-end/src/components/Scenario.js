import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Logout from "./Logout";
import FormInput from "./inputs/FormInput";
import KeyValueInput from "./inputs/KeyValueInput";
import FormTextArea from "./inputs/FormTextArea";
import Swal from 'sweetalert2'
import MockClient from "./util/MockClient";
import GeneralUtil from "./util/GeneralUtil";
import GoogleUser from "./util/GoogleUser";

const scenarioInputId = "scenarioInputId";
const commonPathId = "pathValue";
const commonUrlKeyId = "urlKey";
const commonUrlValueId = "urlValue";
const commonHeaderKeyId = "headerKey";
const commonHeaderValueId = "headerValue";
const requestBodyInputId = "requestBodyInputId";
const responseBodyInputId = "responseBodyInputId";
const get = "GET";
const post = "POST";
const put = "PUT";
const delet = "DELETE";
const normalFormGroupClass = "form-group";
const failedFormGroupClass = "form-group shake has-error";

class Scenario extends Component {

    constructor(props) {
        super(props);

        this.method = get;
        this.specId = GeneralUtil.checkStillLogin(this);
        this.googleUser = GoogleUser.getGoogleUser();

        this.state = {
            pathParamsInputs: [],
            urlParamsInputs: [],
            headers:[],
            showRequestBody: false,
            showResponseBody: false,
            getChecked: true,
            postChecked: false,
            putChecked: false,
            deleteChecked: false,

            scenarioGroupClass: normalFormGroupClass,
            pathParamGroupClass: normalFormGroupClass,
            urlParamGroupClass: normalFormGroupClass,
            headersGroupClass: normalFormGroupClass,
            requestBodyGroupClass: normalFormGroupClass,
            responseBodyGroupClass: normalFormGroupClass
        };

        this.handleSubmit = this.handleSubmit.bind(this);

        this.addPathParams = this.addPathParams.bind(this);
        this.removePathParams = this.removePathParams.bind(this);

        this.addUrlParams = this.addUrlParams.bind(this);
        this.removeUrlParams = this.removeUrlParams.bind(this);

        this.addHeader = this.addHeader.bind(this);
        this.removeHeader = this.removeHeader.bind(this);

        this.addRequestBody = this.addRequestBody.bind(this);
        this.removeRequestBody = this.removeRequestBody.bind(this);

        this.addResponseBody = this.addResponseBody.bind(this);
        this.removeResponseBody = this.removeResponseBody.bind(this);
    }

    validatePathParams(arr) {
        return arr.filter(e => !e.isValid).length > 0
    }

    validateKeyValuePairs(arr) {
        return arr.filter(e => !e.key.isValid || !e.value.isValid).length > 0
    }

    handleSubmit(event) {
        event.preventDefault();

        const scenarioId = FormInput.getValue(scenarioInputId);
        if(!scenarioId.isValid) {
            this.makeScenarioIdGroupFail();
            return;
        }

        const pathResults = this.state.pathParamsInputs.map((e, i) => FormInput.getValue(e + i));
        if(this.validatePathParams(pathResults)) {
            this.makePathParamGroupFail();
            return;
        }

        const urlParams = this.state.urlParamsInputs.map((e, i) => {
            return {
                key: FormInput.getValue(e.k + i),
                value: FormInput.getValue(e.v + i),
            }
        });
        if(this.validateKeyValuePairs(urlParams)) {
            this.urlPathParamGroupFail();
            return;
        }

        const headers = this.state.headers.map((e, i) => {
            return {
                key: FormInput.getValue(e.k + i),
                value: FormInput.getValue(e.v + i),
            }
        });
        if(this.validateKeyValuePairs(headers)) {
            this.headersGroupFail();
            return;
        }

        const reqBody = FormInput.getValue(requestBodyInputId);
        if(this.state.showRequestBody) {
            if(!reqBody.isValid) {
                this.requestBodyGroupFail();
                return;
            }
        }

        const resBody = FormInput.getValue(responseBodyInputId);
        if(this.state.showResponseBody) {
            if (!resBody.isValid) {
                this.responseBodyGroupFail();
                return;
            }
        }

        MockClient.addNewScenario(
            scenarioId.value,
            this.method,
            pathResults.length > 0 ? pathResults.map(e => e.value) : [],
            urlParams.length > 0 ? this.paramsToObject(urlParams) : {},
            headers.length > 0 ? this.paramsToObject(headers) : {},
            reqBody ? reqBody.value : undefined,
            resBody ? resBody.value : undefined,
            (r) => {
                console.log("New scenario was added", r);
                Swal({
                    position: 'center',
                    type: 'success',
                    title: 'New scenario was added',
                    showConfirmButton: false,
                    timer: 2000
                });
                this.props.history.push('/home')
            },
            (e) => {
                console.log("Fail to add new scenario", e);
                GeneralUtil.check403response(e, this);
                this.makeScenarioIdGroupFail();
                this.makePathParamGroupFail();
                this.urlPathParamGroupFail();
                this.headersGroupFail();
                this.requestBodyGroupFail();
                this.responseBodyGroupFail();
            }
        );
    }

    makeScenarioIdGroupFail() {
        this.setState({
            scenarioGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                scenarioGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    makePathParamGroupFail() {
        this.setState({
            pathParamGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                pathParamGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    urlPathParamGroupFail() {
        this.setState({
            urlParamGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                urlParamGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    headersGroupFail() {
        this.setState({
            headersGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                headersGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    requestBodyGroupFail() {
        this.setState({
            requestBodyGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                requestBodyGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    responseBodyGroupFail() {
        this.setState({
            responseBodyGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                responseBodyGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    addPathParams() {
        this.setState(prev => ({
            pathParamsInputs: [...prev.pathParamsInputs, commonPathId]
        }));
    }

    removePathParams() {
        this.setState(prev => {
            prev.pathParamsInputs.pop();
            return {
                pathParamsInputs: prev.pathParamsInputs
            }
        });
    }

    addUrlParams() {
        this.setState(prev => ({
            urlParamsInputs: [...prev.urlParamsInputs, {k: commonUrlKeyId, v: commonUrlValueId}]
        }));
    }

    removeUrlParams() {
        this.setState(prev => {
            prev.urlParamsInputs.pop();
            return {
                urlParamsInputs: prev.urlParamsInputs
            }
        });
    }

    addHeader() {
        this.setState(prev => ({
            headers: [...prev.headers, {k: commonHeaderKeyId, v: commonHeaderValueId}]
        }));
    }

    removeHeader() {
        this.setState(prev => {
            prev.headers.pop();
            return {
                headers: prev.headers
            }
        });
    }

    addRequestBody() {
        this.setState({
            showRequestBody: true
        })
    }

    removeRequestBody() {
        this.setState({
            showRequestBody: false
        })
    }

    addResponseBody() {
        this.setState({
            showResponseBody: true
        })
    }

    removeResponseBody() {
        this.setState({
            showResponseBody: false
        })
    }

    handleRadioChange(method) {
        switch (method) {
            case get : {
                this.method = get;
                this.setState({
                    getChecked: true,
                    postChecked: false,
                    putChecked: false,
                    deleteChecked: false
                });
                break;
            }
            case post : {
                this.method = post;
                this.setState({
                    getChecked: false,
                    postChecked: true,
                    putChecked: false,
                    deleteChecked: false
                });
                break;
            }
            case put : {
                this.method = put;
                this.setState({
                    getChecked: false,
                    postChecked: false,
                    putChecked: true,
                    deleteChecked: false
                });
                break;
            }
            case delet : {
                this.method = delet;
                this.setState({
                    getChecked: false,
                    postChecked: false,
                    putChecked: false,
                    deleteChecked: true
                });
                break;
            }
            default: {
                console.warn("unknown method")
            }
        }
    }

    paramsToObject(params) {
        const arr = params.map(e => {
            return {[e.key.value]: e.value.value};
        });
        const obj = {};
        for(let i = 0; i < arr.length; ++i) {
            let o = arr[i];
            let key = Object.keys(o)[0];
            obj[key] = o[key];
        }
        return obj;
    }

    render() {
        return (
            <div className="container">

                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.googleUser.givenName}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/home">Home</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container-fluid text-center">
                    <div className="row">
                        <div className="container">
                            <h1>New scenario</h1>
                            <form onSubmit={this.handleSubmit} className="form-horizontal">

                                {/* SCENARIO ID */}

                                <div className={this.state.scenarioGroupClass}>
                                    <FormInput
                                        id={scenarioInputId}
                                        labelSize="2"
                                        inputSize="9"
                                        name="ScenarioId"
                                    />
                                </div>

                                {/* METHODS */}

                                <div className="form-group text-left">

                                    <label className="col-sm-2 control-label">Method</label>
                                    <div className="col-sm-9">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            id="method-get"
                                            checked={this.state.getChecked}
                                            onChange={
                                                () => {this.handleRadioChange(get)}
                                            }
                                        />
                                        <label className="form-check-label method-radio control-label" htmlFor="method-get">GET</label>

                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            id="method-post"
                                            checked={this.state.postChecked}
                                            onChange={
                                                () => {this.handleRadioChange(post)}
                                            }
                                        />
                                        <label className="form-check-label method-radio control-label" htmlFor="method-post">POST</label>

                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            id="method-put"
                                            checked={this.state.putChecked}
                                            onChange={
                                                () => {this.handleRadioChange(put)}
                                            }
                                        />
                                        <label className="form-check-label method-radio control-label" htmlFor="method-put">PUT</label>

                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            id="method-delete"
                                            checked={this.state.deleteChecked}
                                            onChange={
                                                () => {this.handleRadioChange(delet)}
                                            }
                                        />
                                        <label className="form-check-label method-radio control-label" htmlFor="method-delete">DELETE</label>
                                    </div>
                                    <div className="col-sm-1">{}</div>
                                </div>

                                {/* PATH PARAMS */}

                                <div className={this.state.pathParamGroupClass}>
                                    <label className="col-sm-2 control-label" htmlFor="addPathParam">Path parameters</label>
                                    <div className="col-sm-1 text-left">
                                        <input
                                            type="button"
                                            className="btn btn-default"
                                            value="add"
                                            onClick={this.addPathParams}
                                        />
                                    </div>
                                    {
                                        this.state.pathParamsInputs.length > 0 &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                            type="button"
                                            className="btn btn-default"
                                            value="remove"
                                            onClick={this.removePathParams}
                                            />
                                        </div>
                                    }
                                    <div className="col-sm-9">{}</div>
                                </div>

                                {
                                    this.state.pathParamsInputs.map((e, i) => {
                                        return  <div className={this.state.pathParamGroupClass}>
                                                    <FormInput
                                                        id={e + i}
                                                        labelSize="3"
                                                        inputSize="8"
                                                        name="segment"
                                                    />
                                                </div>
                                    })
                                }

                                {/* URL PARAMS */}

                                <div className={this.state.urlParamGroupClass}>
                                    <label className="col-sm-2 control-label" htmlFor="addPathParam">Url parameters</label>
                                    <div className="col-sm-1 text-left">
                                        <input
                                            type="button"
                                            className="btn btn-default"
                                            value="add"
                                            onClick={this.addUrlParams}
                                        />
                                    </div>
                                    {
                                        this.state.urlParamsInputs.length > 0 &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                                type="button"
                                                className="btn btn-default"
                                                value="remove"
                                                onClick={this.removeUrlParams}
                                            />
                                        </div>
                                    }
                                    <div className="col-sm-9">{}</div>
                                </div>

                                {
                                    this.state.urlParamsInputs.map((e, i) => {
                                        return  <div className={this.state.urlParamGroupClass}>
                                                    <KeyValueInput
                                                        keyId={e.k + i}
                                                        keyLabelSize="3"
                                                        keyInputSize="3"
                                                        keyName="key"

                                                        valueId={e.v + i}
                                                        valueLabelSize="1"
                                                        valueInputSize="4"
                                                        valueName="value"
                                                    />
                                                </div>
                                    })
                                }

                                {/* HEADERS */}

                                <div className={this.state.headersGroupClass}>
                                    <label className="col-sm-2 control-label" htmlFor="addPathParam">Headers</label>
                                    <div className="col-sm-1 text-left">
                                        <input
                                            type="button"
                                            className="btn btn-default"
                                            value="add"
                                            onClick={this.addHeader}
                                        />
                                    </div>
                                    {
                                        this.state.headers.length > 0 &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                                type="button"
                                                className="btn btn-default"
                                                value="remove"
                                                onClick={this.removeHeader}
                                            />
                                        </div>
                                    }
                                    <div className="col-sm-9">{}</div>
                                </div>

                                {
                                    this.state.headers.map((e, i) => {
                                        return  <div className={this.state.headersGroupClass}>
                                                    <KeyValueInput
                                                        keyId={e.k + i}
                                                        keyLabelSize="3"
                                                        keyInputSize="3"
                                                        keyName="key"

                                                        valueId={e.v + i}
                                                        valueLabelSize="1"
                                                        valueInputSize="4"
                                                        valueName="value"
                                                    />
                                                </div>
                                    })
                                }

                                {/* REQUEST BODY */}

                                <div className={this.state.requestBodyGroupClass}>
                                    <label className="col-sm-2 control-label" htmlFor="addPathParam">Request body</label>
                                    {
                                        !this.state.showRequestBody &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                                type="button"
                                                className="btn btn-default"
                                                value="add"
                                                onClick={this.addRequestBody}
                                            />
                                        </div>
                                    }
                                    {
                                        this.state.showRequestBody &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                                type="button"
                                                className="btn btn-default"
                                                value="remove"
                                                onClick={this.removeRequestBody}
                                            />
                                        </div>
                                    }
                                    <div className="col-sm-9">{}</div>
                                </div>
                                {
                                    this.state.showRequestBody &&
                                    <div className={this.state.requestBodyGroupClass}>
                                        <div className="col-sm-2">{}</div>
                                        <div className="col-sm-9">
                                            <FormTextArea
                                                id={requestBodyInputId}
                                                rows="5"
                                            />
                                        </div>
                                    </div>
                                }

                                {/* RESPONSE BODY */}

                                <div className={this.state.responseBodyGroupClass}>
                                    <label className="col-sm-2 control-label" htmlFor="addPathParam">Response body</label>
                                    {
                                        !this.state.showResponseBody &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                                type="button"
                                                className="btn btn-default"
                                                value="add"
                                                onClick={this.addResponseBody}
                                            />
                                        </div>
                                    }
                                    {
                                        this.state.showResponseBody &&
                                        <div className="col-sm-1 text-left">
                                            <input
                                                type="button"
                                                className="btn btn-default"
                                                value="remove"
                                                onClick={this.removeResponseBody}
                                            />
                                        </div>
                                    }
                                    <div className="col-sm-9">{}</div>
                                </div>
                                {
                                    this.state.showResponseBody &&
                                    <div className={this.state.responseBodyGroupClass}>
                                        <div className="col-sm-2">{}</div>
                                        <div className="col-sm-9">
                                            <FormTextArea
                                                id={responseBodyInputId}
                                                rows="5"
                                            />
                                        </div>
                                    </div>
                                }

                                <div className={normalFormGroupClass}>
                                    <input type="submit" value="Submit" className="btn btn-primary" id="submitScenario"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Scenario;