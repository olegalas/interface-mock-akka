import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Logout from "./Logout";
import MockClient from "./util/MockClient";
import FormInput from "./inputs/FormInput";
import Swal from "./Scenario";
import GeneralUtil from "./util/GeneralUtil";

const normalFormGroupClass = "form-group";
const failedFormGroupClass = "form-group shake has-error";
const emailId = "emailId";

class ChangeEmail extends Component {

    constructor(props) {
        super(props);

        this.specId = GeneralUtil.checkStillLogin(this);

        this.state = {
            emailFormGroupClass: normalFormGroupClass,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.makeEmailGroupFail = this.makeEmailGroupFail.bind(this);
        this.sendChangeEmail = this.sendChangeEmail.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        const email = FormInput.getValue(emailId);
        if(!email.isValid) {
            this.makeEmailGroupFail();
            return;
        }

        this.sendChangeEmail(email.value);
    }

    makeEmailGroupFail() {
        this.setState({
            emailFormGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                emailFormGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    sendChangeEmail(email) {

        MockClient.changeEmail (
            this.specId,
            email,
            (r) => {
                console.log("Email was changed successfully", r);
                Swal({
                    position: 'center',
                    type: 'success',
                    title: 'Email was changed',
                    showConfirmButton: false,
                    timer: 2000
                });
                this.props.history.push('/settings');
            },
            (e) => {
                console.log("Fail change email", e);
                GeneralUtil.check403response(e, this);
                this.makeEmailGroupFail();
            }
        );

    }

    render() {
        return (
            <div className="container">

                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.specId}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/settings">Settings</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container-fluid text-center">
                    <div className="row content">
                        <div className="container">
                            <h1>Change email</h1>
                            <form onSubmit={this.handleSubmit} className="form-horizontal">
                                <div className={this.state.emailFormGroupClass}>
                                    <FormInput
                                        id={emailId}
                                        labelSize="2"
                                        inputSize="9"
                                        name="Email:"
                                    />
                                </div>
                                <input type="submit" value="Submit" className="btn btn-primary"/>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default ChangeEmail;