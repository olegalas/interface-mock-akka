import React, { Component } from 'react';
import {Link} from "react-router-dom";
import MockClient from "./util/MockClient";
import Logout from "./Logout"
import Swal from 'sweetalert2'
import GeneralUtil from "./util/GeneralUtil";
import GoogleUser from "./util/GoogleUser";


class Home extends Component {

    constructor(props) {
        super(props);

        this.specId = GeneralUtil.checkStillLogin(this);
        this.googleUser = GoogleUser.getGoogleUser();

        this.state = {
            scenarios: [],
            method: "-",
            firstSegment: "-",
            segments: [],
            firstParam: "-",
            params: [],
            firstHeader: "-",
            headers: [],
            requestBody: "-",
            responseBody: "-"
        };

        this.onScenarioLiClick = this.onScenarioLiClick.bind(this);
        this.handleRequestBodyChange = this.handleRequestBodyChange.bind(this);
        this.handleResponseBodyChange = this.handleResponseBodyChange.bind(this);
    }

    componentDidMount() {
        if(this.specId) {
            MockClient.getSpec(
                this.specId,
                (r) => {
                    console.log("Got spec", r);

                    this.setState({
                        scenarios: r.specDTO.scenario
                    });

                },
                (e) => {
                    console.log("Fail get spec", e);
                    GeneralUtil.check403response(e, this);
                }
            )
        }
    }

    onScenarioLiClick(scenarioId) {
        if(!scenarioId) {
            console.error("Empty scenarioId ! ");
            return;
        }

        MockClient.getScenario(
            scenarioId,
            (r) => {
                console.log("Got scenario", r);
                
                const s = r.scenario;

                const firstSegment = s.segments instanceof Array ? s.segments[0] ? s.segments.shift() : "-" : "-";

                const params = s.params || {};
                const headers = s.headers || {};
                const paramsKeys = Object.keys(params);
                const headersKeys = Object.keys(headers);
                const paramArr = [];
                for(let i = 0; paramsKeys.length > i; i++) {
                    paramArr.push([paramsKeys[i], params[paramsKeys[i]]])
                }
                const headerArr = [];
                for(let i = 0; headersKeys.length > i; i++) {
                    headerArr.push([headersKeys[i], headers[headersKeys[i]]])
                }

                const firstParam = paramArr[0] ? paramArr[0][0] + "=" + paramArr.shift()[1] : "-";
                const firstHeader = headerArr[0] ? headerArr[0][0] + "=" + headerArr.shift()[1] : "-";

                this.setState({
                    method: s.method,
                    firstSegment: firstSegment,
                    segments: (s.segments instanceof Array ? s.segments : []).map(s =>
                        <tr>
                            <td>{/*empty*/}</td>
                            <td>{s}</td>
                        </tr>
                    ),
                    firstParam: firstParam,
                    params: paramArr.map(s =>
                        <tr>
                            <td>{/*empty*/}</td>
                            <td>{s[0] + "=" + s[1]}</td>
                        </tr>
                    ),
                    firstHeader: firstHeader,
                    headers: headerArr.map(s =>
                        <tr>
                            <td>{/*empty*/}</td>
                            <td>{s[0] + "=" + s[1]}</td>
                        </tr>
                    ),
                    requestBody: s.body ? s.body : "-",
                    responseBody: s.responseBody ? s.responseBody: "-"
                })

            },
            (e) => {
                console.log("Fail get scenario", e);
                GeneralUtil.check403response(e, this);
            }
        );

    }

    handleRequestBodyChange(event) {
        this.setState({
            requestBody: event.target.value
        });
    }

    handleResponseBodyChange(event) {
        this.setState({
            responseBody: event.target.value
        });
    }

    removeScenario(scenarioId) {

        Swal({
            title: 'Are you sure?',
            text: "You won't be able to revert scenario!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                MockClient.removeScenario(
                    scenarioId,
                    (r) => {
                        console.log("Was delete scenario", r);

                        this.setState(prev => {
                            return {
                                scenarios: prev.scenarios.filter(scId => scId !== scenarioId)
                            }
                        });

                    },
                    (e) => {
                        console.log("Fail delete scenario", e);
                        GeneralUtil.check403response(e, this);
                    }
                );

                Swal(
                    'Deleted!',
                    'Scenario has been deleted.',
                    'success'
                )
            }
        })

    }

    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.googleUser.givenName}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/settings">Settings</Link></li>
                                <li><Link to="/about">About</Link></li>
                                <li><Link to="/contacts">Contacts</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid text-center">
                    <div className="row content">
                        <div className="col-sm-2 text-left">
                            <ul className="nav nav-pills nav-stacked" role="tablist">
                                {
                                    this.state.scenarios.map((scenarioId) => {
                                        return  <li
                                                    className="scenario-li"
                                                    onClick={() => this.onScenarioLiClick(scenarioId)}
                                                >
                                                    <Link to="#">{scenarioId}</Link>
                                                    <span
                                                        className="close"
                                                        onClick={(e) => {
                                                                e.stopPropagation();
                                                                this.removeScenario(scenarioId)
                                                            }
                                                        }
                                                    >&times;
                                                    </span>
                                                </li>
                                    })
                                }
                                <li className="active"><Link to="scenario">Add new scenario</Link></li>
                            </ul>
                        </div>
                        <div className="col-sm-10 text-left">
                            <div>
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th className="col-md-2">Parameter</th>
                                            <th className="col-md-10">Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <tr>
                                             <th>Method : </th>
                                             <td>{this.state.method}</td>
                                         </tr>
                                        <tr>
                                            <td>{/*empty*/}</td>
                                            <td>{/*empty*/}</td>
                                        </tr>
                                         <tr>
                                             <th>Segments : </th>
                                             <td>{this.state.firstSegment}</td>
                                         </tr>
                                         {this.state.segments}
                                        <tr>
                                            <td>{/*empty*/}</td>
                                            <td>{/*empty*/}</td>
                                        </tr>
                                         <tr>
                                             <th>URL params : </th>
                                             <td>{this.state.firstParam}</td>
                                         </tr>
                                         {this.state.params}
                                         <tr>
                                             <td>{/*empty*/}</td>
                                             <td>{/*empty*/}</td>
                                         </tr>
                                         <tr>
                                             <th>Headers : </th>
                                             <td>{this.state.firstHeader}</td>
                                         </tr>
                                         {this.state.headers}
                                         <tr>
                                             <td>{/*empty*/}</td>
                                             <td>{/*empty*/}</td>
                                         </tr>
                                         <tr>
                                              <th>Request body :</th>
                                              <textarea disabled value={this.state.requestBody} onChange={this.handleRequestBodyChange}/>
                                         </tr>
                                         <tr>
                                             <td>{/*empty*/}</td>
                                             <td>{/*empty*/}</td>
                                         </tr>
                                         <tr>
                                             <th>Response body :</th>
                                             <textarea disabled value={this.state.responseBody} onChange={this.handleResponseBodyChange}/>
                                         </tr>
                                        <tr>
                                            <td>{/*empty*/}</td>
                                            <td>{/*empty*/}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;