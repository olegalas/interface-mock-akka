import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Logout from "./Logout"
import GeneralUtil from "./util/GeneralUtil";
import GoogleUser from "./util/GoogleUser";


class Settings extends Component {

    constructor(props) {
        super(props);
        GeneralUtil.checkStillLogin(this);
        this.googleUser = GoogleUser.getGoogleUser();
    }

    render() {
        return (
            <div className="container">

                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.googleUser.givenName}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/home">Home</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container-fluid text-center">
                    <div className="row content">
                        <div className="col-sm-2 text-left">
                            <ul className="nav nav-pills nav-stacked" role="tablist">
                                <li><Link to="changepass">Change password</Link></li>
                                <li><Link to="changeemail">Change email</Link></li>
                            </ul>
                        </div>
                        <div className="col-sm-10 text-left">
                            {/* Empty */}
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Settings;