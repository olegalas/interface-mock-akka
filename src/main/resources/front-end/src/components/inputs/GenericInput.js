import React, {Component} from 'react';
import Storage from '../util/Storage';

const invalidCharacters = [':', '/', '?', '#', '[', ']', '@', '!', '$', '&', '\'', '(', ')', '*', '+', ',', ';', '=', ' '];

class FormInputResult {
    constructor(isValid, value) {
        this.isValid = isValid;
        this.value = value;
    }
}

class GenericInput extends Component {
    constructor(props) {
        super(props);
    }

    setValue(id, isValid, v) {
        Storage.setT(id, new FormInputResult(isValid, v));
    }

    static getValue(id) {
        return Storage.getT(id);
    }

    invalidCharacters() {
        return invalidCharacters;
    }
}

export {
    GenericInput,
    FormInputResult
};