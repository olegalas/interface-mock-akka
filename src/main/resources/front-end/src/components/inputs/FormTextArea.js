import React from 'react';
import {GenericInput} from './GenericInput';

class FormTextArea extends GenericInput {

    constructor(props){
        super(props);

        this.isValid = false;

        this.state = {
            id: props.id,
            inputClass: "",
            value: "",
            rows: props.rows,
            errorReason: ""
        };
        this.setValue(props.id, false, "");

        this.handleOnChange = this.handleOnChange.bind(this);
    }

    checkInvalidCharacters(string) {
        return this.invalidCharacters().filter(c => string.includes(c)).length > 0
    }

    handleOnChange(e) {

        const v = e.target.value;
        let inputClass = "";
        let errorReason = "";
        this.isValid = true;

        this.setState({
            value: v
        });
        if(!v) {
            inputClass = "has-error";
            errorReason = "empty";
            this.isValid = false;
        }
        if(this.checkInvalidCharacters(v)) {
            inputClass = "has-error";
            errorReason = "invalid character";
            this.isValid = false;
        }
        this.setState({
            inputClass: inputClass,
            errorReason: errorReason
        });

        this.setValue(this.state.id, this.isValid, v);
    }

    render() {
        return (
            <div className={this.state.inputClass}>
                <textarea
                    className="form-control hide-element"
                    rows={this.state.rows}
                    value={this.state.value}
                    onChange={this.handleOnChange}
                    onBlur={this.handleOnChange}
                />
                <span className="error">{this.state.errorReason}</span>
            </div>
        )
    }

}

export default FormTextArea;