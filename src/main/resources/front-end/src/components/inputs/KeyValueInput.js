import React, {Component} from 'react';
import FormInput from './FormInput';


class KeyValueInput extends Component {

    constructor(props){
        super(props);
        this.state = {
            keyId: props.keyId,
            keyLabelSize: props.keyLabelSize,
            keyInputSize: props.keyInputSize,
            keyName: props.keyName,

            valueId: props.valueId,
            valueLabelSize: props.valueLabelSize,
            valueInputSize: props.valueInputSize,
            valueName: props.valueName,
        };
    }

    render() {
        return (
            <div>
                <FormInput
                    id={this.state.keyId}
                    labelSize={this.state.keyLabelSize}
                    inputSize={this.state.keyInputSize}
                    name={this.state.keyName}
                />
                <FormInput
                    id={this.state.valueId}
                    labelSize={this.state.valueLabelSize}
                    inputSize={this.state.valueInputSize}
                    name={this.state.valueName}
                />
            </div>
        )}

}

export default KeyValueInput;