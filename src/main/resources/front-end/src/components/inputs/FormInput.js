import React from 'react';
import {GenericInput} from './GenericInput';

class FormInput extends GenericInput {

    constructor(props){
        super(props);

        this.isValid = false;
        this.type = props.type ? props.type : "text";

        this.state = {
            id: props.id,
            inputClass: "",
            value: "",
            labelSize: props.labelSize,
            inputSize: props.inputSize,
            name: props.name,
            errorReason: ""
        };
        this.setValue(props.id, false, "");

        this.handleOnChange = this.handleOnChange.bind(this);
    }

    checkInvalidCharacters(string) {
        return this.invalidCharacters().filter(c => string.includes(c)).length > 0
    }

    handleOnChange(e) {

        const v = e.target.value;
        let inputClass = "";
        let errorReason = "";
        this.isValid = true;

        this.setState({
            value: v
        });
        if(!v) {
            inputClass = "has-error";
            errorReason = "empty";
            this.isValid = false;
        }
        if(this.type !== "email" && this.checkInvalidCharacters(v)) {
            inputClass = "has-error";
            errorReason = "invalid character";
            this.isValid = false;
        }
        this.setState({
            inputClass: inputClass,
            errorReason: errorReason
        });

        this.setValue(this.state.id, this.isValid, v);
    }

    render() {
        return (
        <div className={this.state.inputClass}>
            <label
                className={"col-sm-" + this.state.labelSize + " control-label"}
                htmlFor={this.state.id}>
                {this.state.name}
            </label>
            <div className={"col-sm-" + this.state.inputSize}>
                <input
                    type="text"
                    className="form-control is-invalid"
                    value={this.state.value}
                    id={this.state.id}
                    onChange={this.handleOnChange}
                    onBlur={this.handleOnChange}
                />
                <span className="error">{this.state.errorReason}</span>
            </div>
        </div>
    )}

}

export default FormInput;