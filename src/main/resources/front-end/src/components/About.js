import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Logout from "./Logout"
import GeneralUtil from "./util/GeneralUtil";


class About extends Component {

    constructor(props) {
        super(props);
        this.specId = GeneralUtil.checkStillLogin(this);
    }

    render() {
        return (
            <div className="container">

                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.specId}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/home">Home</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>


                <h1>Dex Mock</h1>
                <p>Project for convenient creating mocks very quick spending few minutes.</p>
                <p>This project is written on Scala as back-end language and on React as main front-end tool.</p>
                <p>Currently it contains only 3 microservices:<br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://bitbucket.org/olegalas/quick-mock">quick-mock</a></b> - as core,<br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://bitbucket.org/olegalas/spec-mock">spec-mock</a></b> - as storage service with Redis database,<br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://bitbucket.org/olegalas/interface-mock-akka">interface-mock</a></b> - as service which works with users and has front-end part.<br/>
                </p>
            </div>
        );
    }
}

export default About;