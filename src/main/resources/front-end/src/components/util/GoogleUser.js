import Storage from "./Storage";


class GoogleUser {
    static putGoogleUser(user) {
        Storage.setP("profileObj", user);
    }
    static getGoogleUser() {
        return Storage.getP("profileObj")
    }
}

export default GoogleUser;