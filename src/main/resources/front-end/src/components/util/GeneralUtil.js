import Storage from "./Storage";
import GoogleUser from "./GoogleUser";

class GeneralUtil {
    static check403response(r, component) {
        if(r.status === 403) {
            Storage.removeP("specId");
            component.props.history.push('/')
        }
    }
    static checkStillLogin(component) {
        const specId = Storage.getP("specId");
        const user = GoogleUser.getGoogleUser();
        if(!specId && !user) {
            component.props.history.push('/');
        }
        return specId;
    }
    static checkIfLogin(component) {
        const specId = Storage.getP("specId");
        const user = GoogleUser.getGoogleUser();
        if(specId && user) {
            component.props.history.push('/home')
        }
    }
}

export default GeneralUtil;