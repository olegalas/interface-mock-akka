const temporaryStorage = sessionStorage;
const permanentStorage = localStorage;

class Storage {
    static setT(key, obj) {
        temporaryStorage.setItem(key, JSON.stringify(obj));
    }
    static getT(key) {
        return JSON.parse(temporaryStorage.getItem(key));
    }
    static setP(key, obj) {
        permanentStorage.setItem(key, JSON.stringify(obj));
    }
    static getP(key) {
        return JSON.parse(permanentStorage.getItem(key));
    }
    static removeP(key) {
        permanentStorage.removeItem(key);
    }
}

export default Storage;