import $ from "jquery";

const urlPrefix = process.env.REACT_APP_API_URL;

const makeRequest = (method, url, body, successF, failF) => {
    $.ajax({
        type: method,
        url: url,
        headers: {
            "Content-Type":"application/json"
        },
        data: body? JSON.stringify(body) : null,
        success: successF,
        error: failF,
        xhrFields: {
            withCredentials: true
        }
    });
};

class MockClient {

    static changeEmail(specId, email, successF, failF) {
        makeRequest(
            "POST",
            urlPrefix + "/v1/changeemail",
            {
                specId: specId,
                email: email,
            },
            successF,
            failF
        );
    }

    static changePass(specId, pass, pass2, successF, failF) {
        makeRequest(
            "POST",
            urlPrefix + "/v1/changepass",
            {
                specId: specId,
                pass: pass,
                pass2: pass2
            },
            successF,
            failF
        );
    }

    static forgotPass(email, successF, failF) {
        makeRequest(
            "POST",
            urlPrefix + "/v1/forgotpass",
            {
                email: email
            },
            successF,
            failF
        );
    }

    static registration(login, email, pass, successF, failF) {
        makeRequest(
            "POST",
            urlPrefix + "/v1/registration",
            {
                login: login,
                email: email,
                pass: pass
            },
            successF,
            failF
        );
    }

    static login(tokenId, googleId, successF, failF) {
        makeRequest(
            "POST",
            urlPrefix + "/v1/login",
            {
                googleId: googleId,
                tokenId: tokenId
            },
            successF,
            failF
        );
    }

    static logout(successF, failF) {
        makeRequest(
            "POST",
            urlPrefix + "/v1/logout",
            null,
            successF,
            failF
        );
    }

    static addNewScenario(scenarioId, method, pathParams, urlParams, headers, reqBody, resBody, successF, failF) {
        makeRequest(
            "PUT",
            urlPrefix + "/v1/scenario",
            {
                scenarioId: scenarioId,
                method: method,
                segments: pathParams,
                params: urlParams,
                headers: headers,
                body: reqBody,
                responseBody: resBody
            },
            successF,
            failF
        );
    }

    static getSpec(specId, successF, failF) {
        makeRequest(
            "GET",
            urlPrefix + "/v1/spec/" + specId,
            null,
            successF,
            failF
        );
    }

    static getScenario(scenarioId, successF, failF) {
        makeRequest(
            "GET",
            urlPrefix + "/v1/scenario/" + scenarioId,
            null,
            successF,
            failF
        );
    }

    static removeScenario(scenarioId, successF, failF) {
        makeRequest(
            "DELETE",
            urlPrefix + "/v1/scenario/" + scenarioId,
            null,
            successF,
            failF
        );
    }

}

export default MockClient;