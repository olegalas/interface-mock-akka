import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Logout from "./Logout"
import GeneralUtil from "./util/GeneralUtil";


class Contacts extends Component {

    constructor(props) {
        super(props);
        this.specId = GeneralUtil.checkStillLogin(this);
    }

    render() {
        return (
            <div className="container">

                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.specId}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/home">Home</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>


                <h1>Contacts</h1>
                <p>
                    Here my pages in social networking<br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://www.linkedin.com/in/oleg-vasylyshyn-370472104/">LinkedIn</a></b><br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://www.facebook.com/vasilishin.oleg">FaceBook</a></b><br/>
                </p>
                <p>
                    Also you can contribute and help me.. here links on Bitbucket<br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://bitbucket.org/olegalas/quick-mock">quick-mock</a></b><br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://bitbucket.org/olegalas/spec-mock">spec-mock</a></b><br/>
                    <b><a target='_blank' rel="noopener noreferrer" href="https://bitbucket.org/olegalas/interface-mock-akka">interface-mock</a></b><br/>
                </p>

            </div>
        );
    }
}

export default Contacts;