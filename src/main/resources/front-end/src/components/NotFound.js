import React, { Component } from 'react';
import {Link} from "react-router-dom";


class NotFound extends Component {
    render() {
        return (
            <div className="App">
                <h1>PAGE NOT FOUND</h1>
                <Link to="/">Login</Link>
            </div>
        );
    }
}

export default NotFound;