import React, { Component } from 'react';
import "bootstrap";
import MockClient from "./util/MockClient";
import Storage from "./util/Storage";
import GoogleUser from "./util/GoogleUser";
import GeneralUtil from "./util/GeneralUtil";
import GoogleLogin from 'react-google-login';

class Login extends Component {

    constructor(props) {
        super(props);

        GeneralUtil.checkIfLogin(this);

        this.state = {
            loginFormGroupClass: "form-group",
            passFormGroupClass: "form-group",
        };
        this.sendLogin = this.sendLogin.bind(this);
        this.onS = this.onS.bind(this);
        this.onF = this.onF.bind(this);
    }

    sendLogin(tokenId, googleId) {

        MockClient.login(
            tokenId,
            googleId,
            (r) => {
                console.log("Success login", r);
                Storage.setP("specId", r.specId);
                this.props.history.push('/home')
            },
            (e) => {
                console.log("Fail login", e);
            }
        );

    }

    onS(resp) {
        console.log("success login", resp);
        GoogleUser.putGoogleUser(resp.profileObj);
        this.sendLogin(resp.tokenId, resp.profileObj.googleId);
    }

    onF(resp) {
        console.log("fail");
        console.log(resp);
    }

    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <span className="navbar-brand">DexMock</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid text-center">
                    <h1>Login</h1>
                    <GoogleLogin
                        clientId="861699894384-k97ke6qf3hb4hln0ufml6lj1q5q3k1lj.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={this.onS}
                        onFailure={this.onS}
                        cookiePolicy={'single_host_origin'}
                    />
                </div>
            </div>
        );
    }
}

export default Login;