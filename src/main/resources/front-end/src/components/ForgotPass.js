import React, { Component } from 'react';
import {Link} from "react-router-dom";
import MockClient from "./util/MockClient";
import FormInput from "./inputs/FormInput";
import GeneralUtil from "./util/GeneralUtil";

const normalFormGroupClass = "form-group";
const failedFormGroupClass = "form-group shake has-error";
const emailInputId = "emailInputId";

class ForgotPass extends Component {

    constructor(props) {
        super(props);

        GeneralUtil.checkIfLogin(this);

        this.state = {
            formGroupClass: normalFormGroupClass,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.sendEmail = this.sendEmail.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        const email = FormInput.getValue(emailInputId);
        if(!email.isValid) {
            this.makeEmailGroupFail();
            return;
        }

        this.sendEmail(email.value);
    }

    makeEmailGroupFail() {
        this.setState({
            formGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                formGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    sendEmail(email) {

        MockClient.forgotPass(
            email,
            (r) => {
                console.log("Success");
                this.props.history.push('/')
            },
            (r) => {
                console.log("Fail");
                this.makeEmailGroupFail();
            }
         );

    }

    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <span className="navbar-brand">DexMock</span>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid text-center">
                    <h1>Forgot password?</h1>
                    <p>Please enter your email and we will send you your password</p>
                    <form onSubmit={this.handleSubmit} className="form-horizontal">
                        <div className={this.state.formGroupClass}>
                            <FormInput
                                id={emailInputId}
                                labelSize="2"
                                inputSize="9"
                                name="Email:"
                                type="email"
                            />
                        </div>
                        <input type="submit" value="Submit" className="btn btn-primary"/>
                    </form>
                    <Link to="/">Login</Link>
                    <br/>
                    <Link to="/registration">Registration</Link>
                </div>
            </div>
        );
    }
}

export default ForgotPass;