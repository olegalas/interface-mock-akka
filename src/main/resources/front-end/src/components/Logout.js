import React, { Component } from 'react';
import MockClient from "./util/MockClient";
import {Link} from "react-router-dom";


class Logout extends Component {

    constructor(props) {
        super(props);
        this.history = props.history;
        this.logout = this.logout.bind(this);
    }

    logout() {
        MockClient.logout(
            (ok) => {
                console.log("LOGOUT");
                localStorage.removeItem("specId");
                this.props.history.push('/')
            },
            (e) => {
                console.log("Fail logout", e);
            }
        );
    }

    render() {
        return (

            <li>
                <Link to="#" data-toggle="modal" data-target="#logout-modal">
                    <span className="glyphicon glyphicon-log-in">{/*empty*/}</span> Logout
                </Link>

                <div id="logout-modal" className="modal" tabIndex="-1" role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Logout</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p>Are you sure you want to logout?</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => {
                                    this.logout();
                                }}>Yes</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>

            </li>


        )
    }

}

export default Logout;