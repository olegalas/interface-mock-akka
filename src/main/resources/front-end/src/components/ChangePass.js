import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Logout from "./Logout"
import MockClient from "./util/MockClient";
import FormInput from "./inputs/FormInput";
import Swal from "./Scenario";
import GeneralUtil from "./util/GeneralUtil";

const normalFormGroupClass = "form-group";
const failedFormGroupClass = "form-group shake has-error";
const passwordId = "passwordId";
const password2Id = "password2Id";

class ChangePass extends Component {

    constructor(props) {
        super(props);

        this.specId = GeneralUtil.checkStillLogin(this);

        this.state = {
            passFormGroupClass: normalFormGroupClass,
            pass2FormGroupClass: normalFormGroupClass
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.makePassGroupFail = this.makePassGroupFail.bind(this);
        this.makePass2GroupFail = this.makePass2GroupFail.bind(this);
        this.sendChangePass = this.sendChangePass.bind(this);
    }


    handleSubmit(event) {
        event.preventDefault();

        const pass = FormInput.getValue(passwordId);
        if(!pass.isValid) {
            this.makePassGroupFail();
            return;
        }

        const pass2 = FormInput.getValue(password2Id);
        if(!pass2.isValid) {
            this.makePass2GroupFail();
            return;
        }

        this.sendChangePass(pass.value, pass2.value);
    }

    makePassGroupFail() {
        this.setState({
            passFormGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                passFormGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    makePass2GroupFail() {
        this.setState({
            pass2FormGroupClass: failedFormGroupClass
        });
        setTimeout(() => {
            this.setState({
                pass2FormGroupClass: normalFormGroupClass
            });
        }, 500);
    }

    sendChangePass(pass, pass2) {

        MockClient.changePass(
            this.specId,
            pass,
            pass2,
            (r) => {
                console.log("Password was changed successfully", r);
                Swal({
                    position: 'center',
                    type: 'success',
                    title: 'Password was changed',
                    showConfirmButton: false,
                    timer: 2000
                });
                this.props.history.push('/settings')
            },
            (e) => {
                console.log("Fail change password", e);
                GeneralUtil.check403response(e, this);
                this.makePassGroupFail();
                this.makePass2GroupFail();
            }
        );

    }

    render() {
        return (
            <div className="container">

                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target="#myNavbar">
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                                <span className="icon-bar">{/*empty*/}</span>
                            </button>
                            <span className="navbar-brand">{this.specId}</span>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="active"><Link to="/settings">Settings</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <Logout history={this.props.history}/>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container-fluid text-center">
                    <div className="row content">
                        <div className="container">
                            <h1>Change password</h1>
                            <form onSubmit={this.handleSubmit} className="form-horizontal">
                                <div className={this.state.passFormGroupClass}>
                                    <FormInput
                                        id={passwordId}
                                        labelSize="2"
                                        inputSize="9"
                                        name="Password:"
                                    />
                                </div>
                                <div className={this.state.pass2FormGroupClass}>
                                    <FormInput
                                        id={password2Id}
                                        labelSize="2"
                                        inputSize="9"
                                        name="Password:"
                                    />
                                </div>
                                <input type="submit" value="Submit" className="btn btn-primary"/>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default ChangePass;