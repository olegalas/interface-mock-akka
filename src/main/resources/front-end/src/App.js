import React, { Component } from 'react';
import './css/bootstrap-theme.min.css'
import './css/bootstrap.min.css'
import './css/App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import About from './components/About';
import Contacts from './components/Contacts';
import Login from './components/Login';
import Home from './components/Home';
import NotFound from './components/NotFound';
import ForgotPass from './components/ForgotPass';
import Settings from './components/Settings';
import ChangePass from './components/ChangePass';
import ChangeEmail from './components/ChangeEmail';
import Scenario from './components/Scenario';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
            <Router>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route path="/home" component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/contacts" component={Contacts} />
                    <Route path="/forgotpass" component={ForgotPass} />
                    <Route path="/settings" component={Settings} />
                    <Route path="/changepass" component={ChangePass} />
                    <Route path="/changeemail" component={ChangeEmail} />
                    <Route path="/scenario" component={Scenario} />
                    <Route component={NotFound} />
                </Switch>
            </Router>
        </header>
      </div>
    );
  }
}

export default App;
