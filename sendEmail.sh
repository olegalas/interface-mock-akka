#!/usr/bin/env bash



curl -s --user 'api:xxx' \
    https://api.mailgun.net/v3/xxx.com/messages \
    -F from='Dexter <admin@xxx.com>' \
    -F to=disposition@ukr.net \
    -F subject='Hello' \
    -F text='Testing some Mailgun awesomeness!'
