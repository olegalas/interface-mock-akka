
name := "interface-mock-akka"
version := "0.0.2"
organization := "com.dexmock"


scalaVersion := "2.12.7"

libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.5"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.12"
libraryDependencies += "io.circe" %% "circe-core" % "0.9.3"
libraryDependencies += "io.circe" %% "circe-generic" % "0.9.3"
libraryDependencies += "io.circe" %% "circe-parser" % "0.9.2"
libraryDependencies += "de.heikoseeberger" %% "akka-http-circe" % "1.22.0"
libraryDependencies += "ch.megard" %% "akka-http-cors" % "0.3.1"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.5.12"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"
libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.1.5"
libraryDependencies += "org.typelevel" %% "cats-core" % "1.5.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
libraryDependencies += "org.mockito" % "mockito-all" % "1.9.5" % Test
libraryDependencies += "com.konghq" % "unirest-java" % "2.3.08"
libraryDependencies += "org.hashids" % "hashids" % "1.0.1"


scalacOptions += "-Ypartial-unification"

test in assembly := {}
assemblyJarName in assembly := s"${name.value}-${version.value}.jar"
